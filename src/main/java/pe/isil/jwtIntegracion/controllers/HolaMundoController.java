package pe.isil.jwtIntegracion.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hola")
public class HolaMundoController {

    @GetMapping()
    public String holaMundo(@RequestParam(value = "nombre", defaultValue = "Mundo") String nombre){
        return "Hola " + nombre + " :)";
    }
}
