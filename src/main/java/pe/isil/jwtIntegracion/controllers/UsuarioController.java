package pe.isil.jwtIntegracion.controllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
//librerias de seguridad
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

//librerias de JWT
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pe.isil.jwtIntegracion.models.Usuario;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UsuarioController {


    private String getJWTToken(String nombreUsuario){
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("isilJWT")
                .setSubject(nombreUsuario)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

        return "Bearer " + token;
    }

    @PostMapping("acceso")
    public Usuario login(@RequestBody Usuario usuario, UriComponentsBuilder builder){
        String token = getJWTToken(usuario.getNombreUsuario());

        usuario.setNombreUsuario(usuario.getContrasena());
        usuario.setToken(token);

        return usuario;
    }

    @GetMapping("acceso")
    public Usuario acceso(@RequestParam("nombreUsuario") String nombreUsuario, @RequestParam("contrasena") String contrasena){
        Usuario usuario = new Usuario();
        //implementa la seguirdad la logica de acceso
        String token = getJWTToken(nombreUsuario);
        usuario.setNombreUsuario(nombreUsuario);
        usuario.setToken(token);

        return usuario;
    }


}
